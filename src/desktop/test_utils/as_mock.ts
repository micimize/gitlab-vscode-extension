/**
 * @deprecated use jest.mocked() instead
 */
export const asMock = (mockFn: unknown) => mockFn as jest.Mock;
